#! /usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

debbuild=debian/build-deb
udebbuild=debian/build-udeb

DEB_HOST_ARCH=$(shell dpkg-architecture -qDEB_HOST_ARCH)
CONFFILE=/etc/cdebconf.conf

SIZEOPTFLAG=-Os
SPEEDOPTFLAG=-O2
DEBUG_CONFIGURE_OPT=
ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
	SIZEOPTFLAG=
	SPEEDOPTFLAG=
	DEBUG_CONFIGURE_OPT=--with-debug=yes
endif

build: build-deb build-udeb

build-arch: build
build-indep: build

$(debbuild)/config.status:
	CFLAGS="$(CFLAGS) $(SPEEDOPTFLAG)" \
	dh_auto_configure -B$(debbuild) -- \
		$(DEBUG_CONFIGURE_OPT)

build-deb: build-deb-stamp
build-deb-stamp: $(debbuild)/config.status
	dh_testdir
	dh_auto_build -B$(debbuild)
	touch $@

$(udebbuild)/config.status:
	CFLAGS="$(CFLAGS) $(SIZEOPTFLAG) -fomit-frame-pointer" \
	dh_auto_configure -B$(udebbuild) -- \
		--enable-d-i \
		--without-rpath
		$(DEBUG_CONFIGURE_OPT)

build-udeb: build-udeb-stamp
build-udeb-stamp: $(udebbuild)/config.status
	dh_testdir
	dh_auto_build -B$(udebbuild)
	touch $@

clean:
	dh_testdir
	dh_testroot
	dh_auto_clean -B$(debbuild)
	dh_auto_clean -B$(udebbuild)
	dh_auto_clean
	dh_clean

install: install-arch install-indep

install-arch: build
	dh_testdir
	dh_testroot
	dh_prep
	$(MAKE) -C $(debbuild) install DESTDIR=$(CURDIR)/debian/tmp/deb \
		TARGET=deb
	$(MAKE) -C $(udebbuild) install DESTDIR=$(CURDIR)/debian/tmp/udeb \
		TARGET=udeb
	dh_install -a

install-indep:

# Build architecture-independent files here.
binary-indep: install-indep
	dh_testdir
	dh_testroot
	dh_installchangelogs -i
	dh_installdocs -i
	dh_installdebconf -i
	dh_installdirs -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

# Build architecture-dependent files here.
binary-arch: install-arch
	dh_testdir
	dh_testroot
	dh_installchangelogs -s
	dh_installdocs -s
	dh_installdebconf -s
	dh_installdirs -s
	dh_strip -s
	dh_compress -s
	dh_fixperms -s
	dh_makeshlibs -s
	dh_installdeb -s
	dh_shlibdeps -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
