# This is just the bootstrapping makefile

all: cdebconf

globalmakeflags:
	./configure

cdebconf: globalmakeflags
	$(MAKE) -C src

configure: configure.ac
:autoconf

install:
	$(MAKE) -C src $@

clean: 
	rm -f *-stamp
	[ ! -f globalmakeflags ] || $(MAKE) -C src $@

distclean: clean
	rm -f config.status config.log config.cache globalmakeflags
	rm -rf autom4te.cache
	rm -f src/Makefile

.PHONY: cdebconf clean distclean
